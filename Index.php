<?php
include 'conexion.php';


$sql = "SELECT * FROM imagenes";
$result = mysqli_query($conn, $sql);

?>

<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <style>
        .imagen{
            margin: 0 auto;
        }
        .fotos {
            max-width: 200px;
            max-height: 200px;
            position: relative;
            display: block;
        }

        .detalle{
            text-align: left;
        }


        .detalle:hover, .delete:hover {
            cursor: pointer;
            color: blue !important;

        }

        .formulario{
            width: 600px;
            margin: 0 auto;
        }
        .contenedorimagen{
            height: 500px; width: 1200px;
            margin: 10% auto
        }

    </style>

</head>
<body>

<!--Inicio form-->
<div class="text-center formulario">
    <h2>Introduzca una foto con sus características</h2>
    <form class="formImagen" enctype="multipart/form-data">
        <div class="form-group">
            <label for="titulofoto">Título</label>
            <input type="text" class="form-control" id="titulofoto" name="titulo" placeholder="Titulo">
        </div>
        <div class="form-group">
            <label for="categoriafoto">Categoria</label>
            <input type="text" class="form-control" id="categoriafoto" name="categoria" placeholder="Categoría">
        </div>
        <div class="form-group">
            <label for="descripcionfoto">Descripción</label>
            <textarea class="form-control" id="descripcionfoto" name="descripcion" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="inputimagen">Selecciona la imagen</label>
            <input type="file" class="form-control-file" id="inputimagen" name="foto">
        </div>
        <button type="button" class="btn btn-primary saveImagen">Guardar</button>
        <input type="hidden" name="id" class="idoculta">

    </form>
    <button type="button" class="btn btn-primary ApiCallButton ">Llamada a API externa</button>
</div>


<!--Final de form-->

<!--Inicio Image bank-->
<div>
    <div class="row contenedorimagen">
        <?php
        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {?>
                <div class="col col-md-4 imagen"><h3>Titulo: <?php echo $row['titulo']; ?></h3><br><h5>Categoria :<?php echo $row['categoria'] ?></h5><img src="<?php echo $row['url'] ? : 'https://duncanlock.net/images/posts/better-figures-images-plugin-for-pelican/dummy-200x200.png';  ?>" class="rounded fotos abreImagen"><a data-id="<?php echo $row['id']; ?>" class="detalle">Editar y detalle</a> <a data-id="<?php echo $row['id']; ?>" data-url="<?php echo $row['url']; ?>" class="delete">Borrar</a> </div>
            <?php }
        } else {
            echo "Ningun resultado";
        }
        ?>
    </div>
</div>
<!--Fin Image bank-->


<div class="modal fade" id="detalleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edicion y detalle de Imagen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="formImagen" enctype="multipart/form-data">
                <div class="modal-body">
                        <div class="form-group">
                            <label for="titulofoto">Título</label>
                            <input type="text" class="form-control titulofoto" name="titulo" placeholder="Titulo">
                        </div>
                        <div class="form-group">
                            <label for="categoriafoto">Categoria</label>
                            <input type="text" class="form-control categoriafoto" name="categoria" placeholder="Categoría">
                        </div>
                        <div class="form-group">
                            <label for="descripcionfoto">Descripción</label>
                            <textarea class="form-control descripcionfoto" name="descripcion" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="inputimagen">Selecciona la imagen</label>
                            <input type="file" class="form-control-file" id="inputimagen" name="foto">
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary saveImagen">Guardar los cambios</button>
                    <input type="hidden" name="id" class="idoculta">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="borrarform">
                <input type="hidden" name="id" class="iddelete">
                <input type="hidden" name="urldelete" class="urldelete">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Eliminar imagen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Seguro que quieres borrar dicha imagen?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary borrardef">Borrar</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
</body>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script>

    $( document ).ready(function() {


        // On click del formulario que inserta la imagen

        $(document).on('click','.saveImagen',function () {

            var form = 0;
            var id = 0;
            var formdata = 0;

            id = $(this).parent().find('.idoculta');

            if(id.val()){
                form = $(this).parent().parent();
            }else{
                form = $(this).parent();
            }

            formdata = new FormData(form[0]);

            $.ajax({
                url : 'setImagen.php',
                type : 'POST',
                data : formdata,
                processData: false,
                contentType: false,
                success : function(data) {
                    if(data == 1){
                        location.reload();
                        toastr.success("Guardado correctamente");
                    }
                    if(data == 0){
                        toastr.error('Rellena todos los campos o hay un error con el servidor');
                    }
                    console.log(data);
                },
                error : function(request,error)
                {
                    toastr.error('No se ha podido guardar');
                }
            });


        });
        //DETALLE Y EDITAR LAS CARACTERISTICAS DE LA FOTO

        $(document).on('click','.detalle',function () {

            $('#detalleModal').modal('show');


            console.log();
            $.ajax({
                url : 'getDetail.php',
                type : 'POST',

                data : {
                    id: $(this).data('id')
                },
                success : function(data) {

                    var datos = JSON.parse(data);


                    $('.titulofoto').val(datos.titulo);
                    $('.categoriafoto').val(datos.categoria);
                    $('.descripcionfoto').val(datos.descripcion);
                    $('#detalleModal').find('.idoculta').val(datos.id);


                },
                error : function(request,error)
                {
                    //toastr.error('No se ha podido guardar');
                }
            });


        });

        $(document).on('click','.delete',function () {

            $('#deleteModal .iddelete').val($(this).data('id'));
            $('#deleteModal .urldelete').val($(this).data('url'));
            $('#deleteModal').modal('show');
        });

        $(document).on('click','.borrardef',function () {

            var idborrado = $(this).parent().parent().find('.iddelete').val();
            var urlborrado = $(this).parent().parent().find('.urldelete').val();

            $.ajax({
                url : 'deleteImage.php',
                type : 'POST',

                data : {
                    id: idborrado,
                    url: urlborrado
                },
                success : function(data) {
                    if(data == 1){
                        location.reload();
                        toastr.success("Guardado correctamente");
                    }
                    if(data == 0){
                        toastr.error('Rellena todos los campos o hay un error con el servidor');
                    }

                },
                error : function(request,error)
                {
                    //toastr.error('No se ha podido guardar');
                }
            });
        });

        $(document).on('click','.ApiCallButton',function () {
            $.ajax({
                url : 'ApiCall.php',
                type : 'POST',
                data : "",
                success : function(data) {
                   if(data == 3){
                       toastr.warning("Ya han sido guardados");
                   }else{
                       location.reload();
                       toastr.success("Guardado correctamente");
                   }
                },
            });

        });



    });

</script>
</html>


